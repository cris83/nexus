### Build
```
]# build.sh
```

### Run
```
]# run.sh
```

### Ref
```
https://github.com/sonatype/docker-nexus
```

