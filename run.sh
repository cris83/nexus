#!/bin/bash
echo "# staring nexus..."
docker run -d \
	-p 8081:8081 \
	-v /usr/local/sonatype-work/nexus/storage:/usr/local/sonatype-work/nexus/storage \
	--name nexus \
	sonatype/nexus:oss
